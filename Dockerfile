FROM gitpod/workspace-full

RUN sudo wget https://github.com/neovim/neovim/releases/download/v0.5.0/nvim.appimage -O /opt/nvim.appimage \
  && sudo chmod a+x /opt/nvim.appimage \
  && sudo /opt/nvim.appimage --appimage-extract \
  && sudo rm /opt/nvim.appimage \
  && sudo mkdir -p /opt/nvim \
  && sudo mv squashfs-root/ /opt/nvim \
  && sudo chown -R gitpod:gitpod /opt/nvim \
  && sudo ln -s /opt/nvim/squashfs-root/usr/bin/nvim /usr/bin/nvim \
  && sudo chmod a+x /usr/bin/nvim

COPY ./vscode-neovim.vsix /opt/nvim/vscode-neovim.vsix
